# Ordutegia API

Ordutegia (Basque for schedule) is an API for making JSON schedules human readable.

### Usage

`./run-image-local`

`./curl_sample_json.sh`

The main application logic is in the `app.py` and `utils.py` files.

### Notes

The format chosen for this exercise is well structured and the necessary information can be parsed from it. However, there are some questions that arose while creating the API:
- A format with an integrated, enforced schema would be beneficial when communicating between different services. Some examples are protocol buffer, avro and thrift, where the messages types are defined in separate schema files and the serializing/deserializing is done via generated code.
- Opening hours consist of continuous time windows. If each time object contained both the opening and closing time (instead of them being separate fields), there would be less chance of misinterpretation or of orphaned open/close events.
- If the data is used to carry the information on the current opening hours, using actual UNIX time might be useful. This would make it clear whether the opening hours are generic for the restaurant or if they take into account the possible deviations for the current week, such as bank holidays.