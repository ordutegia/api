from flask import Flask, jsonify, request, abort
from utils import validate_input_json, move_closing_times, format_human_readable_schedule

app = Flask(__name__)

@app.route('/schedule', methods=['POST'])
def post_schedule():
    if not request.json:
        abort(400)

    if not validate_input_json(request.get_json()):
        abort(422)

    schedule = move_closing_times(request.get_json())
    result = format_human_readable_schedule(schedule)
    
    return result, 200, {'Content-Type': 'text/plain'}

if __name__ == '__main__':
    app.run()
