import unittest
import json
from utils import *

sample_input = json.loads('{"monday":[],"tuesday":[{"type":"open","value":36000},{"type":"close","value":64800}],"wednesday":[],"thursday":[{"type":"open","value":36000},{"type":"close","value":64800}],"friday":[{"type":"open","value":36000}],"saturday":[{"type":"close","value":5400},{"type":"open","value":32400},{"type":"close","value":39600},{"type":"open","value":57600},{"type":"close","value":82800}],"sunday":[{"type":"open","value":43200},{"type":"close","value":75600}]}')

sample_input_extra_closing = json.loads('{"monday":[],"tuesday":[{"type":"open","value":36000},{"type":"close","value":64800},{"type":"close","value":67800}],"wednesday":[],"thursday":[{"type":"open","value":36000},{"type":"close","value":64800}],"friday":[{"type":"open","value":36000}],"saturday":[{"type":"close","value":5400},{"type":"open","value":32400},{"type":"close","value":39600},{"type":"open","value":57600},{"type":"close","value":82800}],"sunday":[{"type":"open","value":43200},{"type":"close","value":75600}]}')

sample_input_missing_monday = json.loads('{"tuesday":[{"type":"open","value":36000},{"type":"close","value":64800}],"wednesday":[],"thursday":[{"type":"open","value":36000},{"type":"close","value":64800}],"friday":[{"type":"open","value":36000}],"saturday":[{"type":"close","value":5400},{"type":"open","value":32400},{"type":"close","value":39600},{"type":"open","value":57600},{"type":"close","value":82800}],"sunday":[{"type":"open","value":43200},{"type":"close","value":75600}]}')

sample_schedule = json.loads('{"monday":[],"tuesday":[{"type":"open","value":36000},{"type":"close","value":64800}],"wednesday":[],"thursday":[{"type":"open","value":36000},{"type":"close","value":64800}],"friday":[{"type":"open","value":36000},{"type":"close","value":5400}],"saturday":[{"type":"open","value":32400},{"type":"close","value":39600},{"type":"open","value":57600},{"type":"close","value":82800}],"sunday":[{"type":"open","value":43200},{"type":"close","value":75600}]}')

class TestValidateInputJson(unittest.TestCase):
    def test_valid_input(self):
        self.assertEqual(validate_input_json(sample_input), True)

    def test_missing_day_fails(self):
        self.assertEqual(validate_input_json(sample_input_missing_monday), False)

    def test_sequential_closing_times_fails(self):
        self.assertEqual(validate_input_json(sample_input_extra_closing), False)

class TestMoveClosingTimes(unittest.TestCase):
    def test(self):
        self.assertEqual(1, 1)

class TestFormatTime12h(unittest.TestCase):
    def test_full_hours(self):
        self.assertEqual(format_time_12h(0), '12 AM')
        self.assertEqual(format_time_12h(3600), '1 AM')
        self.assertEqual(format_time_12h(7200), '2 AM')
        self.assertEqual(format_time_12h(39600), '11 AM')
        self.assertEqual(format_time_12h(43200), '12 PM')
        self.assertEqual(format_time_12h(46800), '1 PM')
        self.assertEqual(format_time_12h(50400), '2 PM')
        self.assertEqual(format_time_12h(79200), '10 PM')
        self.assertEqual(format_time_12h(82800), '11 PM')
    
    def test_with_minutes(self):
        self.assertEqual(format_time_12h(60), '12.01 AM')
        self.assertEqual(format_time_12h(36060), '10.01 AM')
        self.assertEqual(format_time_12h(37800), '10.30 AM')
        self.assertEqual(format_time_12h(43140), '11.59 AM')
        self.assertEqual(format_time_12h(46860), '1.01 PM')
    
    def test_with_seconds(self):
        self.assertEqual(format_time_12h(61), '12.01:01 AM')
        self.assertEqual(format_time_12h(36001), '10.00:01 AM')
        self.assertEqual(format_time_12h(43141), '11.59:01 AM')
        self.assertEqual(format_time_12h(86399), '11.59:59 PM')
    
class TestFormatScheduleRow(unittest.TestCase):
    def test_single_period(self):
        self.assertEqual(format_schedule_row(sample_schedule['tuesday']), '10 AM - 6 PM\n')

    def test_multiple_periods(self):
        self.assertEqual(format_schedule_row(sample_schedule['saturday']), '9 AM - 11 AM, 4 PM - 11 PM\n')


if __name__ == '__main__':
    unittest.main()
