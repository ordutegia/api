import json

weekdays = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]

def validate_input_json(content):
    """
    This function validates the input json. 
    The input must contain all the weekdays.
    There must be equal number of "close" and "open" events.
    The "open" and "close" events must alternate
    """
    for day in weekdays:
        if day not in content.keys():
            return False

    for day in weekdays:
        prev_type = ''
        if content[day]:
            for item in content[day]:
                if item['type'] == prev_type:
                    return False
                else:
                    prev_type = item['type']

    return True

def move_closing_times(content):
    """
    The input json contains lists of opening and closing events in UNIX time. 
    This function assigns the "close" events which span the midnight to the previous date 
    """

    prev_day = "sunday"
    for day in weekdays:
        if content[day]:
            if content[day][0]['type'] == 'close':
                content[prev_day].append(content[day][0])
                del content[day][0]
        prev_day = day

    return content

def create_time_ranges(content):
    """
    This function creates a list of time ranges (open, close) for each day to simplify printing
    """
    
    schedule = {}
    for day in weekdays:
        schedule[day] = []
        timerange = []
     
def format_time_12h(unixtime):
    """
    This function formats UNIX time to a 12 hour representation, eg. 12 AM, 1:30 PM or 4:57:23 PM
    The minutes and seconds are shown only if they are not 0
    """

    hours_24 = unixtime // 3600
    minutes = (unixtime - hours_24 * 3600) // 60
    seconds = unixtime - hours_24 * 3600 - minutes * 60
    
    if hours_24 > 12:
        hours = str(int(hours_24 - 12))
    elif hours_24 == 0:
        hours = '12'
    else:
        hours = str(int(hours_24))

    if minutes > 0 and minutes < 10:
        hours += '.0' + str(minutes)
    elif minutes > 10:
        hours += '.' + str(minutes)
    
    if seconds > 0 and minutes == 0:
        hours += '.00'  

    if seconds > 0 and seconds < 10:
        hours += ':0' + str(seconds)
    elif seconds > 10:
        hours += ':' + str(seconds)

    if hours_24 >= 12:
        hours += ' PM'
    else:
        hours += ' AM'
    
    return hours

def format_schedule_row(row):
    """
    This function formats a single row of the printed schedule
    """

    if not row:
        return 'Closed\n'
    else:
        row_string = ''
        n = len(row)
        for i in range(0, n -1):
            row_string += format_time_12h(row[i]['value']) 
            if row[i]['type'] == 'open':
                row_string += ' - '
            else:
                row_string += ', '

        row_string += format_time_12h(row[n - 1]['value']) + '\n'
        
        return row_string


def format_human_readable_schedule(schedule):
    """
    This function formats the full schedule
    """
   
    schedule_string = ''
    
    for day in weekdays: 
        schedule_string += day.capitalize() + ': ' + format_schedule_row(schedule[day])
             
    return schedule_string

